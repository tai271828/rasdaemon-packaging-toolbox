#!/usr/bin/env bash
#
# usage:
#   cmd arm64
#   cmd amd64
#
arch=$1

download_url_base="https://people.canonical.com/~taihsiang/rasdaemon-${arch}"
packages="linux-image-unsigned-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${arch}.deb \
          linux-modules-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${arch}.deb \
          linux-modules-extra-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${arch}.deb \
          rasdaemon_0.6.8-1_arm64.deb"
working_dir="${HOME}"/deployment-rasdaemon

mkdir -p "${working_dir}"

pushd "${working_dir}"

for deb in ${packages}
do
  download_url=${download_url_base}/${deb}
  wget "${download_url}"
done

sudo apt install -y ./*.deb

popd

awk -F\' '/menuentry / {print $2}' /boot/grub/grub.cfg
sudo grub-reboot '1>2'
