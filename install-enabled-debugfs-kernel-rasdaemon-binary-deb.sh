#!/usr/bin/env bash

# Determine the architecture of the host system
ARCH=$(uname -m)

# Set architecture suffix based on the host architecture
if [[ "$ARCH" == "aarch64" ]]; then
    ARCH_SUFFIX="arm64"
elif [[ "$ARCH" == "x86_64" ]]; then
    ARCH_SUFFIX="amd64"
else
    echo "Unsupported architecture: $ARCH"
    exit 1
fi

# Array of URLs to download
DEB_URLS=(
    "http://ftp.us.debian.org/debian/pool/main/r/rasdaemon/rasdaemon_0.8.1-2_${ARCH_SUFFIX}.deb"
    "https://people.canonical.com/~taihsiang/rasdaemon-${ARCH_SUFFIX}/linux-debugfs-enabled/linux-modules-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${ARCH_SUFFIX}.deb"
    "https://people.canonical.com/~taihsiang/rasdaemon-${ARCH_SUFFIX}/linux-debugfs-enabled/linux-modules-extra-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${ARCH_SUFFIX}.deb"
    "https://people.canonical.com/~taihsiang/rasdaemon-${ARCH_SUFFIX}/linux-debugfs-enabled/linux-image-unsigned-5.15.0-27-generic_5.15.0-27.28~d20220513t004517~ab2e786e8b1e_${ARCH_SUFFIX}.deb"
)

# Temporary directory for downloading the files
TEMP_DIR=$(mktemp -d)

# Download each .deb file
for DEB_URL in "${DEB_URLS[@]}"; do
    echo "Downloading $(basename $DEB_URL)..."
    curl -L -f -o "$TEMP_DIR/$(basename $DEB_URL)" "$DEB_URL"
done

# Install the downloaded .deb files using apt
echo "Installing downloaded packages and resolving dependencies..."
sudo apt install -y "$TEMP_DIR"/*.deb

# Clean up the temporary directory
#echo "Cleaning up..."
#rm -rf "$TEMP_DIR"
echo "$TEMP_DIR"

echo "Installation complete."

sudo awk -F\' '/menuentry / {print $2}' /boot/grub/grub.cfg

echo
echo "You may use grub-reboot to reboot too the target kernel,"
echo "or simply make symbolic link to point to the target kernel and naming the symbolic link always sorted as the first item."
echo
echo "grub-reboot example:"
echo "  grub-reboot '1>2'"

