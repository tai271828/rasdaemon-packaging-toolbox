#!/bin/bash
path_build=$1
server_target=$2
file_name=$( basename ${path_build} )
scps="scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"

${scps} ${path_build} ubuntu@${server_target}:/home/ubuntu/${file_name}

