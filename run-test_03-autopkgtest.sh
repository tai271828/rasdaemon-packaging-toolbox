#sudo autopkgtest-build-qemu sid debian-sid.img
# verify:
# $ sudo qemu-system-x86_64 -hda debian-testing.img -nographic
# memory may be needed bigger than the default 1G
# sudo qemu-system-x86_64 -hda debian-testing.img -m 2048 -nographic

autopkgtest ../rasdaemon_0.8.0-1_amd64.deb ./ --apt-upgrade -- qemu ../qemu-autopkgtest-img/debian-sid.img
# $ autopkgtest ../rasdaemon_0.8.0-1_amd64.deb ./ --apt-upgrade --debug --timeout-factor=2.5  -- qemu ../qemu-autopkgtest-img/debian-testing.img
