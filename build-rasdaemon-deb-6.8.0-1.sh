#!/usr/bin/env bash
WORKING_DIR="${WORKING_DIR:-${HOME}/build-rasdaemon-deb}"
RASDAEMON_DEB_SRC_URL=https://salsa.debian.org/tai271828/rasdaemon-upstream-git.git


mkdir -p "${WORKING_DIR}"
pushd "${WORKING_DIR}"
gbp clone ${RASDAEMON_DEB_SRC_URL} rasdaemon

pushd "${WORKING_DIR}"/rasdaemon
# Ubuntu Jammy
# gbp >= 0.9.25

gbp buildpackage --git-ignore-new --git-builder=sbuild

popd
popd
