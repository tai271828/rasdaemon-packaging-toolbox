#!/usr/bin/env bash
#
# build and run fake edac ko in sid QEMU vm
#
set -eo pipefail

adduser --disabled-password --comment "" tai
adduser tai sudo
echo "tai ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/tai

echo "deb-src http://deb.debian.org/debian/ sid main" | tee -a /etc/apt/sources.list

apt update
# linux-headers for build directory of /lib/modules/6.10.3-amd64/build
apt install -y git make kmod dpkg-dev linux-headers-6.10.3-common linux-headers-6.10.3-amd64

