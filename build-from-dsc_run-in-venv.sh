#!/usr/bin/env bash
WORKING_DIR="${WORKING_DIR:-${HOME}/build-rasdaemon-deb}"
RASDAEMON_UPSTREAM_VERSION="0.6.7"
RASDAEMON_DEBIAN_PKG_VERSION_SUFFIX="1"
RASDAEMON_DSC=rasdaemon_${RASDAEMON_UPSTREAM_VERSION}-${RASDAEMON_DEBIAN_PKG_VERSION_SUFFIX}.dsc
RASDAEMON_SRC_TARBALL=rasdaemon_${RASDAEMON_UPSTREAM_VERSION}.orig.tar.bz2
RASDAEMON_DEB_TARBALL=rasdaemon_${RASDAEMON_UPSTREAM_VERSION}-${RASDAEMON_DEBIAN_PKG_VERSION_SUFFIX}.debian.tar.xz
RASDAEMON_DEB_PKG_URL=http://deb.debian.org/debian/pool/main/r/rasdaemon/


mkdir -p "${WORKING_DIR}"
pushd "${WORKING_DIR}"

# get basic debian information
wget ${RASDAEMON_DEB_PKG_URL}/${RASDAEMON_DSC}
wget ${RASDAEMON_DEB_PKG_URL}/${RASDAEMON_SRC_TARBALL}
wget ${RASDAEMON_DEB_PKG_URL}/${RASDAEMON_DEB_TARBALL}

gbp import-dsc ./rasdaemon_${RASDAEMON_UPSTREAM_VERSION}-${RASDAEMON_DEBIAN_PKG_VERSION_SUFFIX}.dsc

set -x
pushd "${WORKING_DIR}"/rasdaemon
# pristine-tar is not necessary in our case but we did it for future management of git
#
# please note /usr/bin/pristine-tar command is a peal tool from the deb package pristine-tar
# and gbp-pristine-tar seems to provide interface to manipulate the /usr/bin/pristine-tar
# if you do not install pristine-tar (the deb package) prior to invoking "gbp pristine-tar" you will get this
# interesting and confusing error message:
#
#  rasdaemon$ gbp pristine-tar commit ./rasdaemon_0.6.7.orig.tar.bz2 --verbose
#  gbp:debug: ['git', 'rev-parse', '--show-cdup']
#  gbp:debug: ['git', 'rev-parse', '--is-bare-repository']
#  gbp:debug: ['git', 'rev-parse', '--git-dir']
#  gbp:debug: ['git', 'ls-tree', '-z', 'upstream/0.6.7', '--']
#  gbp:debug: ['git', 'mktree', '-z']
#  gbp:debug: pristine-tar [] ['commit', './rasdaemon_0.6.7.orig.tar.bz2', 'cd1c9afacf1e127d515dfca1cfa53a68053dd525']
#  gbp:error: Couldn't commit to 'pristine-tar' with upstream 'cd1c9afacf1e127d515dfca1cfa53a68053dd525':
#  execution failed: [Errno 2] No such file or directory: 'pristine-tar'
#
#  "No such file" because you did not install "pristine-tar"!
gbp pristine-tar commit ../rasdaemon_${RASDAEMON_UPSTREAM_VERSION}.orig.tar.bz2

# if you build with ubuntu bionic, when building with sbuild, the bd-uninstallable error may happen to you.
# error message:
#  +------------------------------------------------------------------------------+
#  | Install build-essential                                                      |
#  +------------------------------------------------------------------------------+
#
#
#  Setup apt archive
#  -----------------
#
#  Merged Build-Depends: build-essential, fakeroot
#  Filtered Build-Depends: build-essential, fakeroot
#  dpkg-deb: building package 'sbuild-build-depends-core-dummy' in '/<<BUILDDIR>>/resolver-dUXEXg/apt_archive/sbuild-build-depends-core-dummy.deb'.
#  Can't locate IO/Compress/Gzip.pm in @INC (you may need to install the IO::Compress::Gzip module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.32.1 /usr/local/share/perl/5.32.1 /usr/lib/x86_64-linux-gnu/perl5/5.32 /usr
#  /share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.32 /usr/share/perl/5.32 /usr/local/lib/site_perl) at -e line 4.
#  BEGIN failed--compilation aborted at -e line 4.
#  E: cannot create dummy archive
#  Failed to run apt-ftparchive.
#  E: Setting up apt archive failed
#
#  Setup apt archive
#  -----------------
#
#  Merged Build-Depends: dose-distcheck
#  Filtered Build-Depends: dose-distcheck
#  dpkg-deb: building package 'sbuild-build-depends-dose3-dummy' in '/<<BUILDDIR>>/resolver-dUXEXg/apt_archive/sbuild-build-depends-dose3-dummy.deb'.
#  Can't locate IO/Compress/Gzip.pm in @INC (you may need to install the IO::Compress::Gzip module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.32.1 /usr/local/share/perl/5.32.1 /usr/lib/x86_64-linux-gnu/perl5/5.32 /usr
#  /share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.32 /usr/share/perl/5.32 /usr/local/lib/site_perl) at -e line 4.
#  BEGIN failed--compilation aborted at -e line 4.
#  E: cannot create dummy archive
#  Failed to run apt-ftparchive.
#  E: Setting up apt archive failed
#  E: Failed to explain bd-uninstallable
#
# workaround:
# sudo sbuild-shell unstable
# I: /bin/sh
# # apt install libio-compress-perl dpkg-dev -y
gbp buildpackage --git-ignore-new --git-builder=sbuild

popd
popd
