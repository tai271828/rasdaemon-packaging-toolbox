#!/bin/bash

# prerequisite: make sure you have installed the kernel enabling edac debugfs
#tail -f /var/log/syslog &

pushd "${HOME}"

#git clone git://git.infradead.org/users/mchehab/rasdaemon.git "${HOME}"/rasdaemon
# in case some lab blocks ports
git clone https://github.com/mchehab/rasdaemon.git "${HOME}"/rasdaemon
echo
echo "======================================="
echo "=      injecting fake errors          ="
echo "======================================="
sudo "${HOME}"/rasdaemon/contrib/edac-tests
echo "=              injected.              ="

echo
echo "======================================="
echo "=         rasdaemon errors            ="
echo "======================================="
sudo ras-mc-ctl --errors
echo

echo
echo "======================================="
echo "=         rasdaemon summary           ="
echo "======================================="
sudo ras-mc-ctl --summary
echo

popd
