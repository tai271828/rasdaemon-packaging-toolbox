#!/bin/bash
#
#   reference:
#   https://wiki.archlinux.org/title/Machine-check_exception
#   https://www.setphaserstostun.org/posts/monitoring-ecc-memory-on-linux-with-rasdaemon/
#
WORKING_ROOT="$(dirname "${BASH_SOURCE[0]}")"

pushd "${WORKING_ROOT}"

echo "Basic functioning check"
./run-test_01-show-general-info.sh
echo

echo "Test with fake events..."
./run-test_02-inject-fake-events.sh

popd