#!/usr/bin/env bash
DIR_TOOLBOX_SCRIPTS="${HOME}"/toolbox/scripts
DIR_REPO="${DIR_TOOLBOX_SCRIPTS}"/packaging-deb-packages/rasdaemon-packaging-toolbox
TESTBUILD_PATCH=${DIR_REPO}/0001-bump-version-enable-EDAC-debugging.patch

# replace with toolbox/bin/tai-prepare-build-ubuntu-kernel.sh
#"${DIR_TOOLBOX_SCRIPTS}"/build-linux-kernel-ubuntu-01-prepare.sh
sudo apt update
sudo apt install pkg-config-dbgsym -y

mkdir ${HOME}/build-kernel-jammy
cd ${HOME}/build-kernel-jammy

# Ubuntu-5.15.0-27.28 can't be built successfully with jammy 5.15.0-1042-nvidia
git clone -b Ubuntu-5.15.0-91.101 --depth 1 https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/jammy

cd jammy

git am < "${TESTBUILD_PATCH}"

# after clean we will have untrack debian/changelog
fakeroot debian/rules clean

fakeroot debian/rules binary-generic skipdbg=false
