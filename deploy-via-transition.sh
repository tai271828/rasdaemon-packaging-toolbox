#!/bin/bash
server_build=$1
server_target=$2
path_build=$3
path_target=$4
file_name=$( basename ${path_build} )
path_transition="/tmp/${file_name}"

scps ubuntu@$server_build} ${path_transition}
scps ${path_transition} ubuntu@$server_target}

