#!/usr/bin/env bash
#
# tested distribution: ubuntu focal (20.04)
#
WORKING_ROOT=${HOME}/build-rasdaemon-deb
UPSTREAM_VERSION="0.6.7"
UPSTREAM_VERSION_GIT_TAG=v${UPSTREAM_VERSION}
URL_REMOTE_GIT_REPO="https://salsa.debian.org/tai271828/rasdaemon.git"
URL_REMOTE_GIT_REPO_UPSTREAM="git://git.infradead.org/users/mchehab/rasdaemon.git"
URL_REMOTE_GIT_REPO_WIP="https://salsa.debian.org/tai271828/rasdaemon-git.git"
#DEB_SRC=${WORKING_ROOT}/rasdaemon-${UPSTREAM_VERSION}
DEB_SRC=${WORKING_ROOT}/rasdaemon
DEB_VERSION="${UPSTREAM_VERSION}"-1
DEP_PKG="devscripts git-buildpackage dh-make quilt libsqlite3-dev libgettextpo-dev dh-exec"


sudo apt update
sudo apt install -y ${DEP_PKG}

mkdir -p "${WORKING_ROOT}"
cd "${WORKING_ROOT}" || exit


#############################################################################################
# Fetch new upstream source
#############################################################################################

# Get upstream tarball by download it manually. This is an one-off task.
#
# Fetch new upstream source - Approach I:
# If the upstream branch has been created with the tarball, then we don't need this line otherwise gbp will fail to build
#     dpkg-source: error: several orig.tar files found (./rasdaemon_0.6.7.orig.tar.bz2 and ./rasdaemon_0.6.7.orig.tar.gz) but only one is allowed
#     E: Failed to package source directory /home/ubuntu/build-rasdaemon-deb/rasdaemon-0.6.7
#     gbp:error: 'sbuild' failed: it exited with 1
#
# Please note according to this instruction of Debian Wiki: https://wiki.debian.org/PackagingWithGit
# Since:
#     1. rasdaemon is managed by git https://git.infradead.org/users/mchehab/rasdaemon.git
#     2. "Packages should use Git, as mentioned above. Desirable is being able to use git-buildpackage" https://wiki.debian.org/GitPackaging
#       2-1. "I completely agree with Joey: if upstream is already using Git, there's no reason not to base the Debian
#     packaging on the upstream repository, and many, many reasons to do so." https://www.eyrie.org/~eagle/journal/2013-04/001.html
#     3. "Packages should use git, as mentioned above." from https://wiki.debian.org/DebianMultimedia/DevelopPackaging
#
#wget https://www.infradead.org/~mchehab/rasdaemon/${TARGET_TARBALL_NAME}
#mv ${TARGET_TARBALL_NAME} ${DEB_ORIG_TARBALL_NAME}

# Fetch new upstream source - Approach II:
# Use gbp and debian/watch. See gbp import-orig --pristine-tar --uscan --no-interactive below

# ITA approach I (if we pick up the git packaging flow) (not recommended)
#
# get the upstream source and git repository,
# and then create a debian directory (from scratch or by copying and modifying previous debian directory work from
# ahs3: git@salsa.debian.org:ahs3/rasdaemon.git 0.6.6-2 debian directory)
#
# I don't recommend this approach because I want to reserve the previous ahs3's work history/credit.
#git clone https://salsa.debian.org/tai271828/rasdaemon-deb.git ${DEB_SRC}

# ITA approach II (if we pick up the git packaging flow)
#
# following ahs3's git work, see git@salsa.debian.org:ahs3/rasdaemon.git , and then
# follow the git packaging guide
gbp clone "${URL_REMOTE_GIT_REPO}" "${DEB_SRC}"

cd "${DEB_SRC}" || exit

# equivalent to the combination of the following commands:
#git checkout master
#git pull
gbp pull

git remote update
git fetch origin upstream:upstream pristine-tar:pristine-tar

# create "upstream" branch and commit the tarball by scanning (uscan: upstream scanning)
gbp import-orig --pristine-tar --uscan --no-interactive
#gbp import-orig --upstream-vcs-tag "${UPSTREAM_VERSION_GIT_TAG}"

# apply my previous work by modifying ahs3's debian directory and make sure we are building the target version of
# package properly
git remote add wip "${URL_REMOTE_GIT_REPO_WIP}"
git fetch wip
git cherry-pick 55b4a1ecb666c716804babd5f2b0fbc8281b9463 9edebc3177ecae4c184585c769b376d05945808c ffc3b67f09afe8acaaf0ae8eb1516a5679b2941a

# this tag is not necessary but will be used if we push the code to debian infra (and the infra will build the package
# with the tag information)
# on officially pushing the package, tag with -s (and -a if you want to add meta info to the tag)
#git tag -a -s "debian/${DEB_VERSION}" -m "Debian release ${DEB_VERSION}"
git tag "debian/${DEB_VERSION}" -m "Debian release ${DEB_VERSION}"

#############################################################################################
# Build deb
#############################################################################################

# Build approach I - the "classic" and "fundamental/low-level tool" way
#dh_make --createorig --single --yes --defaultless
#dpkg-buildpackage -us -uc

# Build approach II - the "classic" way but using debbuild wrapper
# works on:
#   ubuntu-mate bionic
#   ubuntu focal
#debuild -us -uc

# Build approach III - build with gbp (recommended "modern" way)
# when using gbp we could build with schroot so we can build deb in debian distribution environment
# works on:
#   ubuntu focal (cloud image) + chroot debian sid
# NOT works on (for package dependency in chroot):
#   ubuntu bionic (cloud image)
#
#       BEGIN failed--compilation aborted at -e line 4.
#       E: cannot create dummy archive
#       Failed to run apt-ftparchive.
#       E: Setting up apt archive failed
#
#       E: Failed to explain bd-uninstallable
#       gbp:error: 'sbuild' failed: it exited with 1
gbp buildpackage --git-ignore-new --git-builder=sbuild

# TESTING the built package
# (X) Ubuntu Focal -  rasdaemon : Depends: libc6 (>= 2.33) but 2.31-0ubuntu9.7 is to be installed
# (O) Ubuntu Jammy
sudo apt install -y ./build-rasdaemon-deb/rasdaemon_0.6.7-1_a??64.deb
#./rasdaemon-show-general-info.sh

#############################################################################################
# Post-work house-cleaning after building deb
#############################################################################################
# cleaning
#git restore ./; git clean -df; rm -f ../rasdaemon-dbgsym_0.6.7-1_amd64.*deb ../rasdaemon_0.6.7-1*