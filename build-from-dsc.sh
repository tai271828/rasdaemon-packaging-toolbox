#!/usr/bin/env bash
WORKING_DIR=${HOME}/build-rasdaemon-deb
SCRIPT_ENTRY_PATH="$( realpath "${BASH_SOURCE[0]}" )"
SCRIPT_DIR="$( dirname "${SCRIPT_ENTRY_PATH}" )"


# install and setup build env
#../packaging-deb-prepare/01-schroot.sh debian arm64 bullseye

sudo apt install -y debhelper quilt libsqlite3-dev libgettextpo-dev autoconf dh-exec
# poetry is not ready around ubuntu jammy. you may prepare your own poetry instead
sudo apt install -y python3-poetry
sudo apt install -y pristine-tar

# will be used by poetry. seems a packaging bug
pip install CacheControl
export PATH=${HOME}/.local/bin:$PATH

mkdir -p "${WORKING_DIR}"

pushd "${WORKING_DIR}"
# update: poetry deb version has been updated to 0.9.27 in debian sid(unstable), bookworm(testing), and bullseye
# (stable), so we don't need this part. We can invoke build-from-dsc_run-in-venv.sh directly.
#
# prepare python venv for the installation of the latest gbp (0.9.25)
# so we can avoid issue like https://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1821761.html
# when importing dsc with an empty directory
#
# tested distribution: ubuntu jammy
poetry init --no-interaction
poetry add gbp
poetry run ${SCRIPT_DIR}/build-from-dsc_run-in-venv.sh
popd
