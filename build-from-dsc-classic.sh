#!/usr/bin/env bash
WORKING_DIR=${HOME}/build-rasdamon-deb
# install and setup build env
#../packaging-deb-prepare/01-schroot.sh debian arm64 bullseye

mkdir -p ${WORKING_DIR}
cd ${WORKING_DIR}

sudo apt install debhelper quilt libsqlite3-dev libgettextpo-dev autoconf dh-exec

wget http://deb.debian.org/debian/pool/main/r/rasdaemon/rasdaemon_0.6.7.orig.tar.bz2
wget http://deb.debian.org/debian/pool/main/r/rasdaemon/rasdaemon_0.6.7-1.debian.tar.xz

cd rasdaemon-0.6.7
dpkg-buildpackage -us -uc
